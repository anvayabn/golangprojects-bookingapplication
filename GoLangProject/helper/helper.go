package helper

import (
	"strings"
)

func ValidateUserInput(firstName string, lastName string, ticketsBought uint, emailId string, numberOfTickets uint) (bool, bool, bool, bool) {
	var isValidName bool = len(firstName) >= 2 && len(lastName) >= 2
	var isValidtickets bool = ticketsBought > 0
	var isValidEmailId bool = strings.Contains(emailId, "@")
	var ticketsAreAvailable bool = numberOfTickets >= ticketsBought
	return isValidName, isValidtickets, isValidEmailId, ticketsAreAvailable
}
