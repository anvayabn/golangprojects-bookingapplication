package main

import (
	"booking-app/helper"
	"fmt"
	"math/rand"
	"sync"
	"time"
)

var numberOfTickets uint = 50
var conferenceName string = "Conference2022"
var bookings = make([]UserData, 0)

type UserData struct {
	FirstName       string
	SecondName      string
	emailID         string
	numberOfTickets uint
}

var wg = sync.WaitGroup{}

func main() {
	//collect the details and the number of tickets

	//	for { for infinite loops use the for loop

	Greet()

	firstName, lastName, emailId, ticketsBought := getUserInput()

	isValidName, isValidtickets, isValidEmailId, ticketsAreAvailable := helper.ValidateUserInput(firstName, lastName, ticketsBought, emailId, numberOfTickets)

	if isValidName && isValidEmailId && isValidtickets && ticketsAreAvailable {
		bookings = bookingLogic(ticketsBought, firstName, lastName, lastName)

		wg.Add(1)                                          //if there is for threads +1 per thread
		go sendTicket(numberOfTickets, firstName, emailId) //using go thread routines for concorrent excecution

		var arrayFirstName []string = FirstNameShow()
		fmt.Printf("The first names of the people who have booked is as below %v\n", arrayFirstName)

	} else {
		userInputInvalid(isValidName, isValidEmailId, isValidtickets, ticketsAreAvailable)

	}

	//	}
	wg.Wait()

}

func Greet() {
	fmt.Printf("Hey User, Hope you are doing Well\n Welcome to %v\n", conferenceName)
	fmt.Printf("\nWe have %v tickets Left\n", numberOfTickets)
	fmt.Printf("\nBook Yourself One, Hurry!!\n")
}
func FirstNameShow() []string {
	var arrayFirstName []string

	for _, bookings := range bookings {
		arrayFirstName = append(arrayFirstName, bookings.FirstName)

	}
	return arrayFirstName

}

func getUserInput() (string, string, string, uint) {
	var firstName string
	var lastName string
	var emailId string
	var ticketsBought uint

	fmt.Printf("\n	Enter your First name\n")
	fmt.Scanln(&firstName)
	fmt.Printf("Enter your Last name\n")
	fmt.Scanln(&lastName)
	fmt.Printf("Enter your emailId\n")
	fmt.Scanln(&emailId)
	fmt.Printf("Enter number of tickets\n")
	fmt.Scanln(&ticketsBought)
	return firstName, lastName, emailId, ticketsBought
}

func userInputInvalid(isValidName bool, isValidEmailId bool, isValidtickets bool, ticketsAreAvailable bool) {
	if !isValidName {
		fmt.Println("You have provided an invalid first or last name")
	}
	if !isValidEmailId {
		fmt.Println("You have provided an invalid email and doesnot contain @")
	}
	if isValidtickets {
		fmt.Println("You have provided an invalid number of tickets, Enter More than 0")
	}
	if !ticketsAreAvailable {
		fmt.Println("Sorry we are out of tickets come back next time")
	}

}

func bookingLogic(ticketsBought uint, firstName string, lastName string, emailId string) []UserData {
	numberOfTickets = (numberOfTickets - ticketsBought)
	fmt.Printf("Thank you for purchasing %v tickets\n", ticketsBought)
	fmt.Printf("Your details are listed as below\n FirstName:%v\n SecondName:%v\n EmailId:%v\n TicketsBought:%v\n", firstName, lastName, emailId, ticketsBought)
	fmt.Printf("Remaing Tickets are: %v\n", numberOfTickets)

	var userData = UserData{
		FirstName:       firstName,
		SecondName:      lastName,
		emailID:         emailId,
		numberOfTickets: ticketsBought,
	}

	bookings = append(bookings, userData)
	fmt.Printf("The Bookings are as follows %v", bookings)
	return bookings

}

func sendTicket(numberOfTickets uint, firstName string, emailId string) {
	time.Sleep(10 * time.Second)
	var ticket = fmt.Sprintf("%v tickets for %v", numberOfTickets, firstName)
	var ticketID int = rand.Intn(500-400) + 1
	fmt.Println("######################################")
	fmt.Printf("Sending Ticket:\n TicketID:%v to emailID:%v\n Your ticket information is below\n %v", ticketID, emailId, ticket)
	fmt.Println("######################################")
	wg.Done()
}
